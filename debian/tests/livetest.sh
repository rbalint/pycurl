#!/bin/sh

for py in $(py3versions -s)
do
    $py --version
    make do-test PYTHON=$py NOSETESTS="nosetests3 -v" PYFLAKES=pyflakes3 PYCURL_VSFTPD_PATH=/usr/sbin/vsftpd
done
